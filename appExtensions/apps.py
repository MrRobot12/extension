from django.apps import AppConfig


class AppextensionsConfig(AppConfig):
    name = 'appExtensions'
