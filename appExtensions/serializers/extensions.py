from rest_framework import serializers
from appExtensions.models.extensions import *
import platform

PLATFORM_RECORDING_NICE = 'Nice'
PLATFORM_RECORDING_WFO_MEDELLIN = 'Wfo Medellin'
PLATFORM_RECORDING_WFO_BOGOTA = 'Wfo Bogota'

PLATFORM_RECORDING_LIST = [
    (PLATFORM_RECORDING_NICE, 'Nice'),
    (PLATFORM_RECORDING_WFO_MEDELLIN, 'Wfo Medellin'),
    (PLATFORM_RECORDING_WFO_BOGOTA, 'Wfo Bogota'),
]

class ExtensionsCreateSerializer(serializers.ModelSerializer):

    def create(self, validated_data):

        num = validated_data.get('extensions')
        plat = validated_data.get('platform')
        valor = num[0:2]

        valor2 = num[0:1]

        if valor == '36':
            validated_data['platformRecording'] = 'nice'
            validated_data['type'] = 'operación'
        elif valor == '38':
            validated_data['platformRecording'] = 'wfo medellín'
            validated_data['type'] = 'operación'
        elif valor == '39':
            validated_data['platformRecording'] = 'wfo bogotá'
            validated_data['type'] = 'operación'
        elif valor2 == '6':
            validated_data['type'] = 'administrativa'
        elif valor2 == '5' and plat == 'Avaya' or plat == 'Asterisk':
            validated_data['type'] = 'admin-operación'
        elif valor == '80':
            validated_data['type'] = 'admin asterisk Medellín'
        elif valor == '81':
            validated_data['type'] = 'admin asterisk Bogotá'

        
        extension = ExtensionsCreate.objects.create(**validated_data)
        return extension

    class Meta():
        model = ExtensionsCreate
        fields = (
            'id',
            'extensions',
            'platform',
            'platformRecording',
            'status',
            'type',
            'dateModification',
            'ticketDeletion',
    
        )
        read_only_fields = ('platformRecording', 'status','type','dateModification','ticketDeletion')
    

class ExtensionsSerializer(serializers.ModelSerializer):

    place = serializers.CharField(read_only=True)

    extensions_id = serializers.PrimaryKeyRelatedField(write_only=True, queryset=ExtensionsCreate.objects.filter(status=False), source='extensions', label="ExtensionsCreate", required=False, allow_null=True)
    
    extensions = ExtensionsCreateSerializer(read_only=True)

    def create(self, validated_data):

        exten = ExtensionsCreate.objects.get(pk=validated_data['extensions'].id)
        exten.status = True
        exten.save()
        
        place = platform.node()
        validated_data['place'] = place
        
        asigancion = Extensions.objects.create(**validated_data)
        return asigancion


    class Meta:
        model = Extensions
        fields = (
            'id',
            'nameClient',
            'headquarters',
            'place',
            'ticketReassign',
            'extensions',
            'extensions_id',
            
        )
        read_only_fields = ('extensions', )



