'''
Filtros para el módulo de usuarios
UserFilter created by Andrés Felipe Blandón Palacio, Noviembre - 2018
'''
from django_filters import rest_framework as filters
from appExtensions.models.extensions import *


class ExtensionsFilter(filters.FilterSet):

    
    class Meta:
        model = Extensions
        fields = {
            'id': ['in'],
            'nameClient': ['icontains', 'exact'],
            'headquarters': ['icontains', 'exact'],
            'extensions': ['exact'],
            'extensions__platform': ['exact', 'icontains'],
            'extensions__type': ['exact', 'icontains'],
            'extensions__status': ['exact', 'icontains'],
        }
       
     

class ExtensionsCreateFilter(filters.FilterSet):

    class Meta:
        model = ExtensionsCreate
        fields = {
            'id': ['in'],
            'platform': ['icontains', 'exact'],
            'type': ['icontains', 'exact'],
            'status': ['exact'],

        }
