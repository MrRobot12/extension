from django.db import models


PLATFORM_AVAYA = 'Avaya'
PLATFORM_GENESYS = 'Genesys'
PLATFORM_ASTERISK = 'Asterisk'

PLATFORM_LIST = [
    (PLATFORM_AVAYA, 'Avaya'),
    (PLATFORM_GENESYS, 'Genesys'),
    (PLATFORM_ASTERISK, 'Asterisk')
]

PLATFORM_RECORDING_NICE = 'Nice'
PLATFORM_RECORDING_WFO_MEDELLIN = 'Wfo Medellin'
PLATFORM_RECORDING_WFO_BOGOTA = 'Wfo Bogota'

PLATFORM_RECORDING_LIST = [
    (PLATFORM_RECORDING_NICE, 'Nice'),
    (PLATFORM_RECORDING_WFO_MEDELLIN, 'Wfo Medellin'),
    (PLATFORM_RECORDING_WFO_BOGOTA, 'Wfo Bogota'),
]

HEADQUARTERS_MEDELLIN = 'Medellin'
HEADQUARTERS_BOGOTA = 'Bogota'

HEADQUARTERS_LIST = [
    (HEADQUARTERS_MEDELLIN, 'Medellin'),
    (HEADQUARTERS_BOGOTA, 'Bogota')
]

class ExtensionsCreate(models.Model):
    extensions = models.CharField(max_length=50, null=False, unique=True, verbose_name='numero extensión')
    platform = models.CharField('plataforma', choices=PLATFORM_LIST, max_length=30)
    platformRecording = models.CharField(verbose_name='Plataformas de grabación', choices=PLATFORM_RECORDING_LIST, max_length=30)
    status = models.BooleanField(default=False)
    type = models.CharField(verbose_name='Tipo', null=False, max_length=50)
    dateModification = models.DateTimeField(("Date"), auto_now = True)
    ticketDeletion = models.CharField(max_length=50, null=True, verbose_name='Ticket eliminacion')

    def __str__(self):
        return self.extensions
        
class Extensions(models.Model):
    extensions = models. OneToOneField(ExtensionsCreate, on_delete=models.CASCADE, unique=True, related_name='Extension')
    nameClient = models.CharField(max_length=50, null=False, verbose_name='Nombre cliente')
    headquarters = models.CharField(verbose_name='Sede', choices=HEADQUARTERS_LIST, max_length=30)
    place = models.CharField(max_length=120, null=False, verbose_name='Puesto')
    ticketReassign = models.CharField(max_length=50, null=True, verbose_name='Ticket reasigno')
    

    def __str__(self):
        return self.nameClient
