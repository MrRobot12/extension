from rest_framework.filters import OrderingFilter
from rest_framework import viewsets, status
from appExtensions.models.extensions import *
from appExtensions.serializers.extensions import *
from appExtensions.filters import ExtensionsFilter, ExtensionsCreateFilter
from rest_framework.response import Response
from django.http import Http404
from rest_framework.decorators import action
from django.core.exceptions import ObjectDoesNotExist

class ExtensionsView(viewsets.ModelViewSet):

    queryset = Extensions.objects.all()
    serializer_class = ExtensionsSerializer
    filter_class = ExtensionsFilter

    # def destroy(self, request, pk=None):
    #     print(request.data)
    #     asing = Extensions.objects.get(id=pk)
    #     exten = ExtensionsCreate.objects.get(id=asing.extensions.id)
    #     exten.status = False
    #     exten.save()

    #     return pk
    #     # return super().destroy(request, pk)
        

    @action(detail=True, methods=['put'], name='Eliminar asignación de extension')
    def delectAsign(self, request, pk=None):
        asing = Extensions.objects.get(id=pk)
        exten = ExtensionsCreate.objects.get(id=asing.extensions.id)
        exten.status = False
        exten.ticketDeletion = request.data['ticketDeletion']
        exten.save()
        
        return super().destroy(request, pk)



class ExtensionsCreateView(viewsets.ModelViewSet):

    queryset = ExtensionsCreate.objects.all()
    serializer_class = ExtensionsCreateSerializer
    filter_class = ExtensionsCreateFilter