'''
Filtros para el módulo de usuarios
UserFilter created by Andrés Felipe Blandón Palacio, Noviembre - 2018
'''
from django_filters import rest_framework as filters
from app.models import User


class UserFilter(filters.FilterSet):
    '''
    Filtro para User y UserData
    '''
    class Meta:
        model = User
        fields = {
            'id': ['in'],
            'first_name': ['icontains', 'exact'],
            'document': ['icontains', 'exact'],
            'last_name': ['icontains', 'exact'],
            'username': ['icontains', 'exact'],
            'email': ['icontains', 'exact'],
            'is_active': ['exact'],
            'login_type': ['exact']
        }
