# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter
from app.models import User
from app.serializer import UserSerializer
from app.filters import UserFilter
from django.contrib.auth import get_user_model
from rest_framework import viewsets

User = get_user_model()


class UserViewSet(viewsets.ModelViewSet):
    '''
    ViewSet para usuarios
    '''

    app_code = 'app'
    permission_code = 'user'
    queryset = User.objects.all().prefetch_related('groups')
    serializer_class = UserSerializer
    filter_class = UserFilter
    search_fields = ('username', 'first_name', 'last_name', 'email')
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

    def create(self, request):
        self.serializer_class = UserSerializer
        return super().create(request)